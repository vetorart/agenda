<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banco_model extends CI_Model {


    function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $dados = array('nome' => $data['nome'], 'telefone' => $data['telefone'], 'celular' => $data['celular']);
        $this->db->insert('contatos', $dados);
    }
    function lista() {
        $this->db->order_by('nome');
        $query = $this->db->get('contatos');
        return $query->result();
    }

    function buscar($dados) {
        $this->db->where('id', $dados['id']);
        $query = $this->db->get('contatos');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id', $data['id']);
        $this->db->set($data);
        return $this->db->update('contatos');
    }

    function deletar($data) {
         $this->db->where('id', $data['id']);
         return $this->db->delete('contatos');
     }
}