<div class="centraliza">
	<div class="row bb">
		<div class="col-12">
			<div class="row">
				<div class="col-9">
					<h1><span class="fa fa-angle-right"></span> AGENDA</h1>
				</div>
				<div class="col-3" align="right">
					<a class="btn" href="<?php echo base_url(); ?>cadastrar"><span class="fa fa-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="row">
		<div class="col-12">
			<ul class="lista">
				<li>
					<div class="row">
						<div class="col-3 titles">nome</div>
						<div class="col-3 titles">telefone</div>
						<div class="col-3 titles">celular</div>
						<div class="col-3 titles">ação</div>
					</div>
				</li>
				<?php foreach ($dados as $dados): ?>
					<li>
						<div class="row">
							<div class="col-3"><?php echo $dados->nome; ?></div>
							<div class="col-3"><?php echo $dados->telefone; ?></div>
							<div class="col-3"><?php echo $dados->celular; ?></div>
							<div class="col-3">
								<a class="acoes" href="<?php echo base_url().'contato/editar/'.$dados->id; ?>"><span class="fa fa-edit"></span></a>
								<a class="acoes" href="<?php echo base_url().'contato/excluir/'.$dados->id; ?>" onclick="return confirm('Deseja deletar: <?php echo $dados->nome;?>')"><span class="fa fa-trash"></span></a>
							</div>
						</div>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
	</div>
</div>