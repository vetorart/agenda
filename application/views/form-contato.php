<?php
    if($id > 0){
      foreach ($dados as $dados) {
        $id = $dados->id;
        $nome = $dados->nome;
        $telefone = $dados->telefone;
        $celular = $dados->celular;
      }    
  }else {
    $id = null;
    $nome = null;
    $telefone = null;
    $celular = null;
  }

  ?>
  <div class="centraliza">
  <div class="row bb">
    <div class="col-12">
      <div class="row">
        <div class="col-9">
          <h1><span class="fa fa-angle-right"></span> CADASTRAR</h1>
        </div>
        <div class="col-3" align="right">
          <a class="btn" href="<?php echo base_url(); ?>">voltar</a>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <br>
  <form action="<?php echo base_url(); ?>contato/salvar" class="container" id="needs-validation" novalidate method="POST">
  <input type="hidden" name="id" value="<?php echo $id; ?>">
  <div class="row">
    <div class="col-md-6 mb-3">
      <label for="validationCustom03">Nome</label>
      <input type="text" name="nome" class="form-control" id="validationCustom03" required value="<?php echo $nome; ?>">
      <div class="invalid-feedback">
        informe o nome.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom04">Telefone</label>
      <input type="text" name="telefone" class="form-control" id="telefone" required value="<?php echo $telefone; ?>">
      <div class="invalid-feedback">
        informe um telefone fixo.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="">Celular</label>
      <input type="text" name="celular" id="celular" class="form-control" value="<?php echo $celular; ?>">
    </div>
  </div>
  <button class="btn btn-primary" type="submit">cadastrar</button>
</form>
  </div>


  <script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    "use strict";
    window.addEventListener("load", function() {
      var form = document.getElementById("needs-validation");
      form.addEventListener("submit", function(event) {
        if (form.checkValidity() == false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add("was-validated");
      }, false);
    }, false);
  }());
  </script>
