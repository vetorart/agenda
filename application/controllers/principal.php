<?php

    /**
     *
     */
    class Principal extends CI_Controller
    {

    /**
    * Layout default utilizado pelo controlador.
    */
    public $layout = 'default';
    /**
    * Titulo default.
    */
    public $title = 'Agenda PHP';

    /**
    * Definindo os css default.
    */
    public $css = array('custom');

    /**
    * Carregando os js default.
    */
    //public $js = array('');

    function __construct()
    {
      parent::__construct();
      

      $this->load->helper(array('form', 'url', 'array', 'date'));
      $this->load->library('form_validation'); 

    }   

    // Metodoo index
    function index()
    {        
        $this->load->model('banco_model');        
        
        $dados = array(
            'tags' => 'agenda php, teste',
            'description' => 'Teste para agenda PHP',
            'menu'  => '0',
            'urlAtual'  => base_url().$this->uri->segment(1)
        );
        $dados['dados'] = $this->banco_model->lista();

        $this->load->view('home', $dados);
    }

    function cadastrar()
    {        
        $this->load->model('banco_model');        
        
        $dados = array(
            'tags' => 'agenda php, teste',
            'description' => 'Teste para agenda PHP',
            'menu'  => '0',
            'id'  => $this->uri->segment(3)
        );
        if ($dados['id'] > 0) {
            $dados['dados'] = $this->banco_model->buscar($dados); 
        }

        $this->load->view('form-contato', $dados);
    }

    function salvar()
    {     
        $this->load->model('banco_model');

        $data['id'] = $this->banco_model->id = $this->input->post('id');
        $data['nome'] = $this->banco_model->nome = $this->input->post('nome');
        $data['telefone'] = $this->banco_model->telefone = $this->input->post('telefone');
        $data['celular'] = $this->banco_model->celular = $this->input->post('celular');

        if($data['id'] > 0){
          $this->banco_model->atualizar($data);
        } else {
          $this->banco_model->insert($data);
        }
                   
        redirect(base_url(), 'refresh');

    }

    function deletar()
    {     
        
        $this->load->model('banco_model');

        $data['id'] = $this->uri->segment(3);

        $this->banco_model->deletar($data);
                   
        redirect(base_url(), 'refresh');

    }


    

    
}