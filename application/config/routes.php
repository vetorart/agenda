<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "principal";
$route['404_override'] = '';

/* BANNERS */
$route['cadastrar'] = "principal/cadastrar";
$route['contato/excluir/(:any)'] = "principal/deletar/$1";
$route['contato/editar/(:any)'] = "principal/cadastrar/$1";
$route['contato/salvar'] = "principal/salvar";



/* End of file routes.php */
/* Location: ./application/config/routes.php */